// the unit test framework
#include "unity.h"

// the header file of the module to be tested
#include "deep_thought.h"

#include <errno.h>

// setUp is run before each test
// this test does not require any setup
// the framework requires this function to be present
void setUp(void)
{
}

// tearDown is run after each test
// this test does not require any teardown
// the framework requires this function to be present
void tearDown(void)
{
}

void deep_thought_can_calculate_the_answer(void)
{
  int const answer = calculate_answer();
  TEST_ASSERT_EQUAL(42, answer);
}

void deep_thought_needs_a_valid_buffer_to_calculate_the_question(void)
{
  int const result = calculate_question(NULL, 42);
  TEST_ASSERT_EQUAL(-1, result);
  TEST_ASSERT_EQUAL_INT(EINVAL, errno);
}

void deep_thought_needs_a_string_buffer_of_at_least_42_characters_to_calculate_the_question(void)
{
  char question[41];
  int const result = calculate_question(question, sizeof question);
  TEST_ASSERT_EQUAL(-1, result);
  TEST_ASSERT_EQUAL_INT(EINVAL, errno);
}

void deep_thought_cannot_calculate_the_question(void)
{
  char question[BUFSIZ];
  int const result = calculate_question(question, BUFSIZ);
  TEST_ASSERT_EQUAL(-1, result);
  TEST_ASSERT_EQUAL_INT(ENOTSUP, errno);
}

int main(void)
{
  UNITY_BEGIN();

  RUN_TEST(deep_thought_can_calculate_the_answer);
  RUN_TEST(deep_thought_needs_a_valid_buffer_to_calculate_the_question);
  RUN_TEST(deep_thought_needs_a_string_buffer_of_at_least_42_characters_to_calculate_the_question);
  RUN_TEST(deep_thought_cannot_calculate_the_question);

  return UNITY_END();
}
