#include "wilsons_algorithm.h"
#include "unity.h"
#include <stdio.h>
#include <stdlib.h>

// Test function for generating a maze
void test_generate_maze() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Check if the maze is generated successfully
    TEST_ASSERT_NOT_NULL(test_maze.grid);
    
    // Check if the grid dimensions match the specified dimensions
    TEST_ASSERT_EQUAL_INT(5, test_maze.width);
    TEST_ASSERT_EQUAL_INT(5, test_maze.height);
    
    // Check if all grid cells are initialized to zero or non-zero
    for (int i = 0; i < test_maze.height; i++) {
        for (int j = 0; j < test_maze.width; j++) {
            TEST_ASSERT_TRUE((test_maze.grid[i][j] >= 0 && test_maze.grid[i][j] <= 15));
        }
    }
    
    // Check if the starting cell is connected to the maze
    bool found_start = false;
    for (int i = 0; i < test_maze.height; i++) {
        for (int j = 0; j < test_maze.width; j++) {
            if (test_maze.grid[i][j] & IN) {
                found_start = true;
                break;
            }
        }
        if (found_start) {
            break;
        }
    }
    TEST_ASSERT_TRUE(found_start);
}

// Test function for generating a maze with small dimensions
void test_generate_maze_small_dimensions() {
    // Generate a maze with small dimensions
    struct Maze test_maze = generate_maze(1, 1);
    
    // Check if the maze is generated successfully
    TEST_ASSERT_NOT_NULL(test_maze.grid);
    
    // Check if the grid dimensions match the specified dimensions
    TEST_ASSERT_EQUAL_INT(1, test_maze.width);
    TEST_ASSERT_EQUAL_INT(1, test_maze.height);
}

// Test function for generating a maze with large dimensions
void test_generate_maze_large_dimensions() {
    // Generate a maze with large dimensions
    struct Maze test_maze = generate_maze(100, 100);
    
    // Check if the maze is generated successfully
    TEST_ASSERT_NOT_NULL(test_maze.grid);
    
    // Check if the grid dimensions match the specified dimensions
    TEST_ASSERT_EQUAL_INT(100, test_maze.width);
    TEST_ASSERT_EQUAL_INT(100, test_maze.height);
}

// Test function for freeing the maze grid
void test_free_maze() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Free the maze grid
    free_maze(&test_maze);
    
    // Check if the grid is freed successfully
    TEST_ASSERT_NULL(test_maze.grid);
}

// Test function for displaying the maze
void test_display_maze() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Display the maze
    display_maze(test_maze, -1, -1); // Assuming cursor is not within the maze
}

// Test function for displaying the maze with cursor at different positions
void test_display_maze_with_cursor() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Display the maze with cursor at different positions
    for (int x = 0; x < test_maze.width; x++) {
        for (int y = 0; y < test_maze.height; y++) {
            display_maze(test_maze, x, y);
        }
    }
}

// Test function for displaying the maze with cursor outside the maze
void test_display_maze_cursor_outside() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Display the maze with cursor outside the maze
    display_maze(test_maze, -1, -1); // Assuming cursor is not within the maze
}

// Test function for generating and displaying the maze
void test_generate_and_display_maze() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Display the maze
    display_maze(test_maze, -1, -1); // Assuming cursor is not within the maze
}

// Main function to run all the tests
int main() {
    UNITY_BEGIN();
    // Run the test functions
    RUN_TEST(test_generate_maze);
    RUN_TEST(test_generate_maze_small_dimensions);
    RUN_TEST(test_generate_maze_large_dimensions);
    RUN_TEST(test_free_maze);
    RUN_TEST(test_display_maze);
    RUN_TEST(test_display_maze_with_cursor);
    RUN_TEST(test_display_maze_cursor_outside);
    RUN_TEST(test_generate_and_display_maze);
    return UNITY_END();
}