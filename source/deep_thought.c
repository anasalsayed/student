#include "deep_thought.h"

#include <errno.h>

int calculate_answer(void)
{
  return 42;
}

int calculate_question(char question[], size_t length)
{
  if (question == NULL || length < 42)
  {
    errno = EINVAL;
    return -1;
  }

  // we don't have nearly enough computing power to determine the question
  // return an error until we can build a computer the size of a planet
  errno = ENOTSUP;
  return -1;
}
