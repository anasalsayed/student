#include "wilsons_algorithm.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

// Constants to describe the passage directions
#define N 1
#define S 2
#define E 4
#define W 8
#define IN 16

// Function prototypes
static int** allocate_grid(int width, int height);
static void walk(struct Maze *maze);

// Function to generate the maze using Wilson's algorithm
struct Maze generate_maze(int width, int height) {
    // Create a maze structure
    struct Maze maze;
    maze.width = width;
    maze.height = height;

    // Allocate memory for the maze grid
    maze.grid = allocate_grid(width, height);

    // Initialize grid with zeros
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            maze.grid[i][j] = 0;
        }
    }

    srand(time(NULL));

    // Wilson's algorithm to generate the maze
    walk(&maze);

    return maze;
}

// Function to allocate memory for the maze grid
static int** allocate_grid(int width, int height) {
    // Allocate memory for the rows
    int **grid = malloc(height * sizeof(int *));
    if (grid == NULL) {
        exit(EXIT_FAILURE);
    }

    // Allocate memory for each row
    for (int i = 0; i < height; i++) {
        grid[i] = calloc(width, sizeof(int));
        if (grid[i] == NULL) {
            for (int j = 0; j < i; j++) {
                free(grid[j]);
            }
            free(grid);
            exit(EXIT_FAILURE);
        }
    }

    return grid;
}

// Function to perform Wilson's algorithm for maze generation
static void walk(struct Maze *maze) {
    int width = maze->width;
    int height = maze->height;
    int **grid = maze->grid;

    // Main loop of Wilson's algorithm
    while (1) {
        // Choose a random starting point
        int cx = rand() % width;
        int cy = rand() % height;
        if (grid[cy][cx] != 0) {
            continue;
        }

        // Initialize the visit record
        bool **visits = malloc(height * sizeof(bool *));
        if (visits == NULL) {
            exit(EXIT_FAILURE);
        }
        for (int i = 0; i < height; i++) {
            visits[i] = calloc(width, sizeof(bool));
            if (visits[i] == NULL) {
                for (int j = 0; j < i; j++) {
                    free(visits[j]);
                }
                free(visits);
                exit(EXIT_FAILURE);
            }
        }
        visits[cy][cx] = true;

        int start_x = cx, start_y = cy;
        int walking = 1;

        // Random walk loop
        while (walking) {
            walking = 0;
            int dirs[] = {N, S, E, W};
            for (int i = 0; i < 4; i++) {
                int dir = dirs[rand() % 4];
                int nx = cx + (dir == E) - (dir == W);
                int ny = cy + (dir == S) - (dir == N);
                if (nx >= 0 && nx < width && ny >= 0 && ny < height) {
                    visits[cy][cx] = true;
                    if (grid[ny][nx] != 0) {
                        break;
                    } else {
                        cx = nx;
                        cy = ny;
                        walking = 1;
                        break;
                    }
                }
            }
        }

        // Trace back the path
        int x = start_x, y = start_y;
        while (1) {
            int dir = rand() % 4 + 1;
            int nx = x + (dir == E) - (dir == W);
            int ny = y + (dir == S) - (dir == N);
            if (nx >= 0 && nx < width && ny >= 0 && ny < height && visits[ny][nx]) {
                while (x != nx || y != ny) {
                    maze->grid[y][x] |= dir;
                    x += (dir == E) - (dir == W);
                    y += (dir == S) - (dir == N);
                }
                break;
            }
        }

        // Free memory allocated for visit record
        for (int i = 0; i < height; i++) {
            free(visits[i]);
        }
        free(visits);
    }
}

// Function to display the maze in ASCII art
void display_maze(struct Maze maze, int cx, int cy) {// move to upper-left
    printf("\033[H");
    printf(" ");
    for (int i = 0; i < maze.width * 2 - 1; i++) {
        printf("_");
    }
    printf("\n");

    for (int y = 0; y < maze.height; y++) {
        printf("|");
        for (int x = 0; x < maze.width; x++) {
            if (cx == x && cy == y) {
                printf("\033[43m"); // cursor is yellow
            }
            if (maze.grid[y][x] == 0 && y + 1 < maze.height && maze.grid[y + 1][x] == 0) {
                printf(" ");
            } else {
                printf((maze.grid[y][x] & S) != 0 ? " " : "_");
            }
            printf("\033[0m"); // reset color

            if (maze.grid[y][x] == 0 && x + 1 < maze.width && maze.grid[y][x + 1] == 0) {
                printf((y + 1 < maze.height && (maze.grid[y + 1][x] == 0 || maze.grid[y + 1][maze.grid[y + 1][x + 1]] == 0)) ? " " : "_");
            } else {
                printf((maze.grid[y][x] & E) != 0 ? " " : "|");
            }
        }
        printf("\n");
    }
}

// Function to free memory allocated for the maze grid
void free_maze(struct Maze *maze) {
    // Free memory for each row
    for (int i = 0; i < maze->height; i++) {
        free(maze->grid[i]);
    }

    // Free memory for the grid itself
    free(maze->grid);
}

// Test function for displaying the maze with cursor at different positions
void test_display_maze_with_cursor() {
    // Generate a maze
    struct Maze test_maze = generate_maze(5, 5);
    
    // Display the maze with cursor at different positionsSure 
    for (int x = 0; x < test_maze.width; x++) {
        for (int y = 0; y < test_maze.height; y++) {
            display_maze(test_maze, x, y);
        }
    }
}

