#include "wilsons_algorithm.h"

int main() {
    // Generate a maze with dimensions 10x10
    struct Maze maze = generate_maze(10, 10);

    // Display the generated maze
    display_maze(maze, -1, -1);

    // Free the memory allocated for the maze
    free_maze(&maze);

    return 0;
}

