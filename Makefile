# simple Makefile for a small C project
# the project includes a library, an executable and a test program

CC := clang

# directories
BLD_DIR := build
SRC_DIR := source
TST_DIR := test

# make sure the build directory exists
$(shell mkdir -p $(BLD_DIR))

# include directories
INCLUDES := -I./include

# enable various compiler flags
# -std=c17: use the C17 standard
# -Wall: enable all warnings
# -Wextra: enable extra warnings
# -Wpedantic: enable pedantic warnings
# -Werror: treat warnings as errors
# -g: include debugging information in the executable
CFLAGS := \
	-std=c17 \
	-Wall -Wextra -Wpedantic -Werror \
	-g \
	$(INCLUDES)

# we build a static library
LIB := $(BLD_DIR)/libdeepthought.a
LIB_SRC := $(SRC_DIR)/deep_thought.c
LIB_OBJ := $(LIB_SRC:.c=.o)

# and an executable
EXE := $(BLD_DIR)/wilsons
EXE_SRC := \
	$(SRC_DIR)/main.c \
	$(SRC_DIR)/wilsons_algorithm.c
EXE_OBJ := $(EXE_SRC:.c=.o)

# and a test program
TST := $(BLD_DIR)/test
TST_SRC := \
	$(TST_DIR)/test.c \
	$(TST_DIR)/unity.c
TST_OBJ := $(TST_SRC:.c=.o)

# build everything by default
all: $(LIB) $(EXE) $(TST)

# clean up object files and the build directory
clean:
	@rm -vf $(LIB_OBJ) $(EXE_OBJ) $(TST_OBJ)
	@rm -rvf $(BLD_DIR)

# the actual build rules
$(LIB): $(LIB_OBJ)
	$(AR) rcs $(LIB) $(LIB_OBJ)

$(EXE): $(EXE_OBJ) $(LIB)
	$(CC) $(CFLAGS) -o $(EXE) $(EXE_OBJ) $(LIB)

$(TST): $(TST_OBJ) $(LIB)
	$(CC) $(CFLAGS) -o $(TST) $(TST_OBJ) $(LIB) 

.PHONY: all clean