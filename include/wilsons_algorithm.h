#ifndef WILSONS_ALGORITHM_H
#define WILSONS_ALGORITHM_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

// Struct to represent the maze
struct Maze {
    int width;
    int height;
    int **grid;
};

/**
 * @brief Generate a wrap-around maze of arbitrary size using Wilson's algorithm.
 * 
 * @param width Width of the maze.
 * @param height Height of the maze.
 * @return struct Maze The generated maze.
 */
struct Maze generate_maze(int width, int height);

/**
 * @brief Display the maze in ASCII art.
 * 
 * @param maze The maze structure.
 * @param cx Current x-coordinate of the cursor.
 * @param cy Current y-coordinate of the cursor.
 */
void display_maze(struct Maze maze, int cx, int cy);

/**
 * @brief Free the memory allocated for the maze grid.
 * 
 * @param maze Pointer to the maze structure.
 */
void free_maze(struct Maze *maze);

#endif /* WILSONS_ALGORITHM_H */

