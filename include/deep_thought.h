#pragma once

#include <stddef.h>

/**
 * Calculate the answer to the Ultimate Question
 * of Life, The Universe, and Everything.
 **/
int calculate_answer(void);

/**
 * Calculate the Ultimate Question of Life, The Universe, and Everything.
 * @param question The buffer to store the question.
 * @param length The length of the question. Must be at least 42.
 *               At most `length` characters will be written to `question`,
 *               including the null terminator.
 * @returns The number of characters written to `question`.
 *          In case of an error, -1 is returned and errno is set.
 **/
int calculate_question(char question[], size_t length);
